
#ifndef INTF_H
#define INTF_H

#include <unistd.h>
#include <sys/syscall.h>
#include <linux/tsnif.h>

#include "list.h"
#include "trans.h"

enum {
	/* no state - notify state */
	TSNIF_INTF_STATE_NONE = 0,
	/* created, added to the list */
	TSNIF_INTF_STATE_NEW = 1,
	/* attach is sent, waiting for TGROUP msg */
	TSNIF_INTF_STATE_ATTACH,
	/* detach is sent, waiting for ack */
	TSNIF_INTF_STATE_DETACH,
	/* got TGROUP msg, receiving DATA msg */
	TSNIF_INTF_STATE_DATA,
	/* got detach ack, released */
	TSNIF_INTF_STATE_DONE,
};

struct tsnif_term {
	int type;
	int idx;
	int state;

	struct list_head list;
	struct tsnif_handle *handle;
};

struct tsnif_data {
	void *ptr;
	int len;
	uint flags;
	struct timespec time;
	struct winsize ws;
};

typedef int(*cb_data_t)(struct tsnif_term *term, struct tsnif_data *data);
typedef int(*cb_err_t)(struct tsnif_term *term, int err);
typedef int(*cb_release_t)(struct tsnif_term *term);
typedef int(*cb_notify_t)(struct tsnif_term *term, int action);

struct tsnif_ops {
	cb_data_t	cb_data;
	cb_err_t	cb_err;
	cb_release_t	cb_release;
	cb_notify_t	cb_notify;
};

struct tsnif_handle {
	struct list_head terms;
	struct tsnif_ops *ops;
	struct trans_handle trans;
};

typedef int(*cb_tsnif_enum_t)(struct tsnif_term *term);

/* handle functions */
int tsnif_init(struct tsnif_handle *h, struct tsnif_ops *ops);
int tsnif_close(struct tsnif_handle *h);
int tsnif_process(struct tsnif_handle *h);
int tsnif_fd(struct tsnif_handle *h);
int tsnif_list(struct tsnif_handle *h);

/* term functions */
int tsnif_term_add(struct tsnif_handle *h, struct tsnif_term *term,
		int type, int idx);
int tsnif_term_del(struct tsnif_handle *h, struct tsnif_term *term);
int tsnif_attach(struct tsnif_term *term);
int tsnif_detach(struct tsnif_term *term);

#define tsnif_for_each(term, t, handle) \
	list_for_each_entry_safe(term, t, (&handle->terms), list)

#endif /* !INTF_H */
