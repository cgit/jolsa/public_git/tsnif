
#include <stdio.h>
#include <sys/select.h>
#include <unistd.h>
#include <termios.h>
#include <getopt.h>
#include <errno.h>
#include <setjmp.h>
#include <signal.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <time.h>

#include "autoconf.h"
#include "intf.h"
#include "misc.h"
#include "debug.h"

#include "tsnifd.h"

static struct tsnif_handle handle;

static int foreground  = 0;
static int killed      = 0;

static int terminal_add(struct tsnif_term *term)
{
	struct terminal *t;
	int err;

	TSNIF_DEBUG(APP, "type %d, idx %d\n", term->type, term->idx);

	/* XXX debug gets only PTY XXX */
	if (tsnif_debug) {
		if (term->type != TSNIF_TYPE_PTY)
			return 0;
	}

	t = malloc(sizeof(*t));
	if (!t)
		return -ENOMEM;

	memset(t, 0x0, sizeof(*t));

	err = tsnif_term_add(&handle, &t->term, term->type, term->idx);
	if (err) {
		free(t);
		return err;
	}

	err = storage_init(t);
	if (err) {
		tsnif_term_del(&handle, &t->term);
		free(t);
		return err;
	}

	return tsnif_attach(&t->term);
}

static int terminal_del(struct tsnif_term *term)
{
	struct terminal *t;

	TSNIF_DEBUG(APP, "type %d, idx %d\n", term->type, term->idx);

	tsnif_term_del(&handle, term);

	t = container_of(term, struct terminal, term);
	storage_close(t);
	free(t);

	return 0;
}

static int data_cb(struct tsnif_term* term, struct tsnif_data *data)
{
	struct terminal *t;

	t = container_of(term, struct terminal, term);
	return storage_data(t, data);
}

static int err_cb(struct tsnif_term *term, int err)
{
	return terminal_del(term);
}

static int release_cb(struct tsnif_term *term)
{
	return terminal_del(term);
}

static int notify_cb(struct tsnif_term *term, int action)
{
	int err = 0;

	switch(action) {
	case TSNIF_CMD_TTY_RELEASE:
		printf("not watched terminal released - type %d, idx %d\n",
			term->type, term->idx);
		break;

	case TSNIF_CMD_TTY_CREATE:
	case TSNIF_CMD_TTY_LIST:
		err = terminal_add(term);
		break;

	default:
		err = -EINVAL;
	}
	return err;
}

static int release_terms(void)
{
	struct tsnif_term *term, *t;

	tsnif_for_each(term, t, (&handle)) {
		/* The ACK action (release cb) is never going
		 * to happen, since we are going down, * so we
		 * need to run terminal_del manually */
		tsnif_detach(term);
		terminal_del(term);
	}

	return 0;
}

struct tsnif_ops ops = {
	.cb_data	= data_cb,
	.cb_err		= err_cb,
	.cb_release	= release_cb,
	.cb_notify	= notify_cb,
};

static void sig_handler(int sig)
{
	printf("killed\n");
	killed = 1;
}

static void usage()
{
	printf("tsnifd [-fhsdv]\n");
	_exit(-1);
}

static int get_args(int argc, char **argv)
{
	int ret = 0;

	while (1) {
		int c;
		int option_index = 0;
		static struct option long_options[] = {
			{"version", no_argument, 0, 'v'},
			{"foreground", no_argument, 0, 'f'},
			{"store-dir", required_argument, 0, 's'},
			{"version", no_argument, 0, 'V'},
			{"verbose", required_argument, 0, 'v'},
			{"debug", required_argument, 0, 'd'},
			{"help", no_argument, 0, 'h'},
			{0, 0, 0, 0}
		};

		c = getopt_long(argc, argv, "fshd:v:V",
			long_options, &option_index);

		if (c == -1)
			break;

		switch (c) {
		case 'f':
			foreground = 1;
			break;

		case 's':
			storage_dir = optarg;
			break;

		case 'V':
			print_version(argv[0]);
			break;

		case 'v':
			ret = debug_parse_flags(0, optarg);
			break;

		case 'd':
			ret = debug_parse_flags(1, optarg);
			break;

		case 'h':
			usage();
			break;

		default:
			printf("unknown option '%c'", c);
		} /* switch(c) */
	} /* while(1) */

	return ret;
}

int main(int argc, char **argv)
{
	int err, ret;
	jmp_buf env;

	if (get_args(argc, argv))
		usage();

	err = tsnif_init(&handle, &ops);
	if (err)
		return err;

	if ((ret = setjmp(env))) {

		if (ret > 1)
			release_terms();

		tsnif_close(&handle);
		printf("done err = %d\n", err);
		return err;
	}

	err = tsnif_list(&handle);
	if (err)
		longjmp(env, 1);

	signal(SIGINT, sig_handler);

	while(!killed) {
		fd_set rfds;
		struct timeval tv = { 1, 0};
		int ts_fd = tsnif_fd(&handle);

		FD_ZERO(&rfds);
		FD_SET(ts_fd, &rfds);

		err = select(ts_fd + 1, &rfds, NULL, NULL, &tv);
		if (err == -1) {
			perror("select()");
			continue;
		} else if (!err)
			continue;

		if (FD_ISSET(ts_fd, &rfds)) {
			err = tsnif_process(&handle);
			if (err)
				longjmp(env, 2);
		}
	}

	longjmp(env, 2);
}
