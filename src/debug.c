
#include <getopt.h>
#include <asm/errno.h>
#include <string.h>

#include "debug.h"

uint32_t tsnif_debug;

static int enable_flag(int debug, char *flag)
{
	int i;

	for(i = 0; i < FLAGS_MAX; i++) {
		char *f = debug_flag_name[i];

		if (!strcmp(flag, f))
			break;
	}

	if (FLAGS_MAX != i) {
		if (!debug)
			i += 16;

		tsnif_debug |= BIT2NUM(i);
		return 0;
	}

	if (!strcmp(flag, "all")) {
		tsnif_debug |= debug ? 0xffff : 0xffff0000;
		return 0;
	}

	return -EINVAL;
}

int debug_parse_flags(int debug, char *flags)
{
	char *flag;
	int ret = 0, in = 0;

	for(;(flag = strtok(flags, ",")) ;flags = NULL, in++)
		ret |= enable_flag(debug, flag);

	return in ? ret : -EINVAL;
}
