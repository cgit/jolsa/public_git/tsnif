
#ifndef TRANS_H
#define TRANS_H

#include <sys/ioctl.h>

struct trans_handle;
struct trans_msg;

typedef int(*trans_cb_t)(struct trans_handle *h, struct trans_msg *msg);

struct trans_msg {
	/* netlink command */
	int cmd;

	/* common attributes */
	int type;
	int idx;

	/* flags indicating error/ack message */
	int err;
	int ack;

	/* attributes based on cmd field */
	union {
		/* data message */
		struct {
			void *ptr;
			int   len;
			uint  flags;
			struct timespec time;
			struct winsize ws;
		} data;
		/* notify message */
		int action;
		/* error message */
		int error;
		/* group message */
		int group;
	};
};

int trans_init(struct trans_handle *h, trans_cb_t cb);
int trans_close(struct trans_handle *h);
int trans_process(struct trans_handle *h);
int trans_send(struct trans_handle *h, struct trans_msg *msg);
int trans_group(struct trans_handle *h, int group);
int trans_fd(struct trans_handle *h);

/* TODO make CONFIG option and ifdef this place */
#include "trans-libnl.h"

#endif /* !TRANS_H */
