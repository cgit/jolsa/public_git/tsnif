
#ifndef FSM_H
#define FSM_H

int fsm_process(struct tsnif_handle *h,
		struct tsnif_term *term,
		struct trans_msg *msg);

int fsm_send(struct tsnif_term *term, int cmd);

#endif /* !FSM_H */
