#ifndef TSNIFD_H
#define TSNIFD_H

#include "storage.h"
#include "intf.h"

extern char *storage_dir;

struct terminal {
	/* interface term */
	struct tsnif_term term;

	/* storage */
	char *file;
	struct tsnif_storage_opts   storage_opts;
	struct tsnif_storage_handle storage_handle;
};

int storage_init(struct terminal *t);
int storage_close(struct terminal *t);
int storage_data(struct terminal *t, struct tsnif_data *data);

#endif /* TSNIFD_H */
