
#ifndef TRANS_LIBNL_H
#define TRANS_LIBNL_H

struct trans_handle {
	struct nl_handle *sock;
	int family;

	trans_cb_t cb;
};

#endif /* !TRANS_LIBNL_H */
