#ifndef DEBUG_H
#define DEBUG_H

#include <unistd.h>
#include <sys/syscall.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

extern uint32_t tsnif_debug;

#define BIT2NUM(bit) (1 << bit)

enum {
	DEBUG_STORAGE	= BIT2NUM(0),
	DEBUG_FSM	= BIT2NUM(1),
	DEBUG_INTF	= BIT2NUM(2),
	DEBUG_TRANS	= BIT2NUM(3),
	DEBUG_APP	= BIT2NUM(4),
	FLAGS_MAX	= 5,
	VERBOSE_STORAGE	= BIT2NUM(16),
	VERBOSE_FSM	= BIT2NUM(17),
	VERBOSE_INTF	= BIT2NUM(18),
	VERBOSE_TRANS	= BIT2NUM(19),
	VERBOSE_APP	= BIT2NUM(20),
};

static char *debug_flag_name[FLAGS_MAX] = {
	"storage",
	"fsm",
	"intf",
	"trans",
	"app",
};

static inline void debug_print(int flag, char *file, int line,
			       const char *func, char *fmt, ...)
{
#define MAXBUF 256
	char buf[MAXBUF];
	int name_idx;
	va_list ap;

	if (!(flag & tsnif_debug))
		return;

	name_idx = (ffs(flag) % 16) - 1;

	if (flag < BIT2NUM(16)) {
		snprintf(buf, MAXBUF, "%s [%3d:%s:%05d  %s] %s",
			debug_flag_name[name_idx],
			(pid_t) syscall(SYS_gettid),
			file,
			line,
			func,
			fmt);
	} else {
		snprintf(buf, MAXBUF, "[%s] %s",
			debug_flag_name[name_idx],
			fmt);
	}

	va_start(ap, fmt);
	vfprintf(stdout, buf, ap);
	va_end(ap);
}


#define TSNIF_DEBUG(flag, fmt, ...) \
	debug_print(DEBUG_##flag, __FILE__, __LINE__, __FUNCTION__, \
			fmt, ##__VA_ARGS__)

#define TSNIF_VERBOSE(flag, fmt, ...) \
	debug_print(VERBOSE_##flag, __FILE__, __LINE__, __FUNCTION__, \
			fmt, ##__VA_ARGS__)

int debug_parse_flags(int debug, char *flags);

#endif /* DEBUG_H */
