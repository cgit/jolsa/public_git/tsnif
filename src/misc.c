
#include <termios.h>
#include <unistd.h>

int set_term(int set_init)
{
	struct termios new_settings;
	static struct termios init_settings;

	if (set_init)
		return tcsetattr(0, TCSANOW, &init_settings);

	tcgetattr(0, &init_settings);
	new_settings = init_settings;
	new_settings.c_lflag &= ~ICANON;
	new_settings.c_lflag &= ~ECHO;

	return tcsetattr(0, TCSANOW, &new_settings);
}
