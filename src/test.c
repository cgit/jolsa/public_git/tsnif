
#include <stdio.h>
#include <unistd.h>
#include <getopt.h>

#include "debug.h"

#define TEST_RUN(test) \
do { \
	printf("RUNNING " # test "\n"); \
	if ((test())) { \
		printf("FAILED test " # test "\n"); \
		goto out; \
	} \
} while(0)

int assert_debug = 0;

static void usage()
{
	printf("tsnif-test [-d]\n");
	_exit(-1);
}

static int get_args(int argc, char **argv)
{
	int debug = 0;

	while (1) {
		int c;
		int option_index = 0;
		static struct option long_options[] = {
			{"debug", no_argument, 0, 'd'},
			{0, 0, 0, 0}
		};

		c = getopt_long(argc, argv, "d",
			long_options, &option_index);

		if (c == -1)
			break;

		switch (c) {
		case 'd':
			debug++;
			break;

		default:
			printf("unknown option '%c'", c);
		} /* switch(c) */
	} /* while(1) */

	if (debug > 1)
		tsnif_debug = 1;

	if (debug > 0)
		assert_debug = 1;

	return 0;
}

/* storage-mmap.o */
int test_storage_mmap_init(void);
int test_storage_mmap_open(void);
int test_storage_mmap_write_single_chunk(void);
int test_storage_mmap_write_multi_chunks(void);
int test_storage_mmap_read_single_chunk(void);
int test_storage_mmap_read_multi_chunks(void);

int main(int argc, char **argv)
{
	if (get_args(argc, argv))
		usage();

	TEST_RUN(test_storage_mmap_init);
	TEST_RUN(test_storage_mmap_open);
	TEST_RUN(test_storage_mmap_write_single_chunk);
	TEST_RUN(test_storage_mmap_write_multi_chunks);
	TEST_RUN(test_storage_mmap_read_single_chunk);
	TEST_RUN(test_storage_mmap_read_multi_chunks);

	printf("PASSED\n");
	return 0;

out:
	printf("FAILED\n");
	return -1;
}
