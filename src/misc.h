#ifndef MISC_H
#define MISC_H

#include <unistd.h>
#include "autoconf.h"

int set_term(int set_init);

static inline void print_version(char *prog)
{
	printf("%s "CONFIG_TSNIF_VER"\n", prog);
	_exit(0);
}

#endif /* !MISC_H */
