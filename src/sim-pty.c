
#define _XOPEN_SOURCE

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <getopt.h>

int readonly = 0;
int verbose = 0;
int no_sleep = 0;
int sleep_us = 100;


int write_pts(void)
{
	int mfd, sfd;
	char *sname;
	u_int i;

	if ((mfd = getpt()) < 0) {
		perror("getpt failed");
		return -1;
	}

	if (unlockpt(mfd) != 0) {
		perror("unlockpt failed");
		return -1;
	}

	if ((sname = (char*)ptsname(mfd)) == NULL) {
		perror("ptsname");
		return -1;
	}

	printf("got slave pty %s\n", sname);

	if ((sfd = open(sname, O_RDWR)) < 0) {
		perror("open failed");
		return -1;
	}

	fcntl(sfd, F_SETFL, O_NONBLOCK);

	for(i = 0; ; i++) {
		char buf[10000];
		int len;

		if (!readonly) {
			sprintf(buf, "kravaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa%03ddebil\n", i);

			if (-1 == write(sfd, buf, strlen(buf))) {
				perror("write failed");
				return -1;
			}
		}

		buf[0] = 0x0;

		if (-1 == (len = read(mfd, buf, sizeof(buf)))) {
			perror("write failed");
			return -1;
		}
		buf[len] = 0x0;

		if (verbose)
			printf("master %s got: [%s]\n", sname, buf);

		if (!no_sleep)
			usleep(sleep_us);
	}

	close(mfd);

	return 0;
}

int write_file(char *name)
{
	int fd;
	u_int i;

	if ((fd = open(name, O_RDWR)) < 0) {
		perror("open failed");
		return -1;
	}
	
	fcntl(fd, F_SETFL, O_NONBLOCK);

	for(i = 0; ; i++) {
		char buf[10000];

		if (!readonly) {
			sprintf(buf, "kravaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa%03d\n", i);

			if (-1 == write(fd, buf, strlen(buf))) {
				perror("write failed");
				return -1;
			}
		}

		if (!no_sleep)
			usleep(sleep_us);
	}

	close(fd);

	return 0;
}

int main(int argc, char **argv)
{
	char *file = NULL;

	while (1) {
		int c;
		int option_index = 0;
		static struct option long_options[] = {
			{"read", no_argument , 0, 'r'},
			{"verbose", no_argument , 0, 'v'},
			{"f", no_argument , 0, 'f'},
			{0, 0, 0, 0}
		};

		c = getopt_long(argc, argv, "rvns:f:", long_options, 
				&option_index);

		if (c == -1)
			break;

		switch (c) {
		case 'r':
			readonly = 1;
			break;
		case 'v':
			verbose = 1;
			break;
		case 'n':
			no_sleep = 1;
			break;
		case 's':
			sleep_us = atoi(optarg);
			break;
		case 'f':
			file = optarg;
			break;
		default:
			break;
		}
	}

	if (file)
		return write_file(file);

	return write_pts();
}
