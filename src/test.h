
#ifndef TEST_H
#define TEST_H

#include <stdio.h>
#include "intf.h"
#include "debug.h"

extern int assert_debug;

#define TEST_ASSERT(cond, out_label) \
do { \
	if (assert_debug) \
		printf("  ASSERT '" #cond "' ... "); \
	if (tsnif_debug) \
		printf("\n"); \
	if (!(cond)) { \
		rc = -1; \
		printf("FAILED\n"); \
		if (assert_debug) \
			printf("  ^^^^^^ %s:%d function %s\n",\
				__FILE__, __LINE__, __FUNCTION__); \
		goto out_label; \
	} \
	if (assert_debug) \
		printf("OK\n"); \
} while(0)

#endif /* TEST_H */
