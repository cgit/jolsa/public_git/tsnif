#ifndef STORAGE_H
#define STORAGE_H

#include <stdlib.h>
#include <stdint.h>
#include <sys/ioctl.h>

struct tsnif_storage_handle;
struct tsnif_storage_opts;

enum {
	TSNIF_STORAGE_TYPE_MMAP = 1,
};

#define TSNIF_HEADER_MAGIC	0xdeadbeaf
#define TSNIF_STORAGE_VERSION	0x1

struct tsnif_storage_header {
	uint32_t magic;
	uint32_t version;
	uint32_t storage_type;
	uint32_t term_type;
};

enum {
	TSNIF_STORAGE_OPT_READ   = 0x1,
	TSNIF_STORAGE_OPT_WRITE  = 0x2,
};

enum {
	TSNIF_STORAGE_READ_NEXT = 0x1,
	TSNIF_STORAGE_READ_PREV = 0x2,
	TSNIF_STORAGE_READ_EOF  = 0x3,
};

struct tsnif_storage_rec {
	void *ptr;
	int len;
	uint flags;
	struct timespec time;
	struct winsize ws;
};

int tsnif_storage_init(struct tsnif_storage_handle *h,
		       struct tsnif_storage_opts *ops, char *name);
int tsnif_storage_close(struct tsnif_storage_handle *h);
int tsnif_storage_write(struct tsnif_storage_handle *h,
			struct tsnif_storage_rec *rec);
int tsnif_storage_read(struct tsnif_storage_handle *h, int what,
			struct tsnif_storage_rec *rec);
int tsnif_storage_term_type(struct tsnif_storage_handle *h);

/* TODO make CONFIG option and ifdef this place */
#include "storage-mmap.h"

#endif /* STORAGE_H */
