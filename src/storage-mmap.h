
#ifndef STORAGE_MMAP_H
#define STORAGE_MMAP_H

#include <sys/ioctl.h>

struct tsnif_storage_header_mmap {
	struct tsnif_storage_header common;
	/* max size of the file */
	uint32_t size_max;
	/* number of records stored */
	uint32_t cnt;
	/* offset of the first chunk */
	uint32_t offset_start;
};

struct tsnif_storage_rec_mmap {
	uint32_t len;
	uint32_t flags;
	struct timespec time;
	struct winsize ws;
	unsigned char data[];
};

struct tsnif_storage_chunk_header {
	uint32_t free;
	uint32_t cnt;
	unsigned char rec0[];
};

struct tsnif_storage_opts {
	int size_max;
	uint flags;
	int chunk_size;
	int term_type;
};

struct tsnif_storage_chunk {
	struct tsnif_storage_chunk_header *header;
	off_t     offset;
	void     *current_data;
	uint32_t *current_index;

	/* for read interface only */
	int read_index;
};

struct tsnif_storage_handle {
	int fd;

	struct tsnif_storage_opts *opts;

	/* file header */
	struct tsnif_storage_header_mmap *header;

	/* active chunk */
	struct tsnif_storage_chunk active_chunk;

	/* for READ mode only */
	off_t file_size;

};

#endif /* STORAGE_MMAP_H */
