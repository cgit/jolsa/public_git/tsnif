#ifndef TSNIF_H
#define TSNIF_H

/* TODO there has to be better way */
#ifndef __KERNEL__
#include <netlink/attr.h>
#endif

#define TSNIF_VERSION	1
#define TSNIF_OK	1

/* attributes */
enum {
	TSNIF_ATTR_UNSPEC,
	TSNIF_ATTR_TYPE,
	TSNIF_ATTR_IDX,
	TSNIF_ATTR_GROUP,
	TSNIF_ATTR_ERR,
	TSNIF_ATTR_DATA,
	TSNIF_ATTR_WS,
	TSNIF_ATTR_TIME,
	TSNIF_ATTR_FLAGS,
	__TSNIF_ATTR_MAX,
};

#define TSNIF_ATTR_MAX (__TSNIF_ATTR_MAX - 1)

#define TSNIF_POLICY(var) \
static struct nla_policy var[TSNIF_ATTR_MAX + 1] = { \
	[TSNIF_ATTR_TYPE]	= { .type = NLA_U32 }, \
	[TSNIF_ATTR_IDX]	= { .type = NLA_U32 }, \
	[TSNIF_ATTR_GROUP]	= { .type = NLA_U32 }, \
	[TSNIF_ATTR_FLAGS]	= { .type = NLA_U32 }, \
	[TSNIF_ATTR_DATA]	= { .type = NLA_UNSPEC}, \
	[TSNIF_ATTR_TIME]	= { .type = NLA_UNSPEC}, \
	[TSNIF_ATTR_WS]		= { .type = NLA_UNSPEC}, \
};

enum {
	TSNIF_CMD_ATTACH,
	TSNIF_CMD_DETACH,
	TSNIF_CMD_MGROUP,
	TSNIF_CMD_TGROUP,
	TSNIF_CMD_DATA,
	TSNIF_CMD_TTY_CREATE,
	TSNIF_CMD_TTY_RELEASE,
	TSNIF_CMD_TTY_LIST,
	__TSNIF_CMD_MAX,
};

#define TSNIF_CMD_MAX (__TSNIF_CMD_MAX - 1)

enum {
	TSNIF_FLAGS_PTY_MASTER,
	TSNIF_FLAGS_PTY_SLAVE,
};

enum {
	TSNIF_TYPE_TTY,
	TSNIF_TYPE_TTYS,
	TSNIF_TYPE_PTY,
	TSNIF_TYPE_MAX,
};

#endif /* TSNIF_H */
