
export TSNIF_INCLUDE  = $(CURDIR)/include
export TSNIF_AUTOCONF = $(TSNIF_INCLUDE)/autoconf.make
include $(TSNIF_AUTOCONF)

# looks like DESTDIR is a standard, but prioritize ROOTDIR anyway
ifdef DESTDIR
ifndef ROOTDIR
ROOTDIR=$(DESTDIR)
endif
endif


ifneq ($(findstring $(MAKEFLAGS),w),w)
PRINT_DIR = --no-print-directory
else # "make -w"
NO_SUBDIR = :
endif

# nice output definition
# Mostly copied from kernel and git makefiles.

ifndef V
	QUIET_CC          = @echo "  CC" $@;
	QUIET_LD          = @echo "  LD" $@;
	QUIET_LEX         = @echo "  LE" $@;
	QUIET_YACC        = @echo "  YA" $@;
	QUIET_DEP         = @echo "  DEP" $@;
	QUIET_GEN         = @echo "  GEN" $@;
	QUIET_ASCIIDOC    = @echo "  ASCIIDOC" $@;
	QUIET_XMLTO       = @echo "  XMLTO" $@;

define install
	@echo -n "  INSTALL " `echo $(ROOTDIR)$2/$(notdir $1) | sed 's:[/]\+:/:g'` ; \
	mkdir -p $(ROOTDIR)$2; \
	install -m "ugo+$3" $1 $(ROOTDIR)$2; echo
endef
define remove
	@echo "  CLEAN  " $1; $(RM) -rf $1
endef
else
define remove
	$(RM) -rf $1
endef
define install
	mkdir -p $(ROOTDIR)$2; \
	install -m "ugo+$3" $1 $(ROOTDIR)$2; echo
endef
endif


.PHONY: all clean install release snapshot package tags cscope .FORCE-TSNIF-CFLAGS

all::

ifneq ($(MAKECMDGOALS),clean)
-include deps.make
endif

# main building schema
# Module subdir (src) are supposed to fill PROGRAMS and
# OBJS variables, and rule to link the module. The top makefile
# will do the rest.

PROGRAMS=
OBJS=

include src/Makefile

ALL_CFLAGS=-fPIC -Wall -Isrc -D_GNU_SOURCE -I$(TSNIF_INCLUDE)

%.o: %.c TSNIF-CFLAGS
	$(QUIET_CC)$(CC) -o $*.o -c $(ALL_CFLAGS) $<

all:: $(PROGRAMS) TSNIF-CFLAGS

clean::
	$(call remove, $(OBJS) $(PROGRAMS))
	$(call remove, deps.make)

mrproper::
	git clean -xdf

snapshot:
	@$(MAKE) $(PRINT_DIR) package PKG_VER=$(CONFIG_TSNIF_VER)-`date "+%m%d%Y"`

release:
	@$(MAKE) $(PRINT_DIR) package PKG_VER=$(CONFIG_TSNIF_VER)

package:
	$(QUIET_PKG)rm -f tsnif-$(PKG_VER); ln -s . tsnif-$(PKG_VER); \
	echo "tsnif-$(PKG_VER)"; \
	for i in `find . -type f | cut -c 3- | grep -v git`; do \
		git checkout $$i > /dev/null 2>&1; \
		if [ x"$$?" == x"0" ]; then \
			echo "tsnif-$(PKG_VER)/$$i"; \
		fi; \
	done | tar cjvf tsnif-$(PKG_VER).tar.bz2 -T- > /dev/null 2>&1

# dependencies
# The gcc -M depedencies generation needs to repaired to include
# subdirectory name within the target.. at least I haven't find any
# gcc option to do that.

deps.make:
	$(QUIET_DEP)$(RM) -f deps.make; \
	(for obj in $(OBJS); do \
	src=`echo $$obj | sed "s/\.o/.c/"`; \
	$(CC) $(ALL_CFLAGS) -M -MT$$obj $$src;  \
	done) > deps.make

# utilities
tags:
	$(QUIET_GEN)$(RM) -f tags; \
	$(FIND) . -name '*.[hc]' -print | xargs ctags -a

cscope:
	$(QUIET_GEN)$(RM) -f cscope*; \
	$(FIND) . -name '*.[hc]' -print > cscope.files; \
	cscope -b -icscope.files

# detect prefix and cflags changes
TRACK_CFLAGS = $(subst ','\'',$(ALL_CFLAGS)):\
             $(prefix):$(exec_prefix):$(bindir):$(libdir):$(sysconfdir) #'

TSNIF-CFLAGS: .FORCE-TSNIF-CFLAGS
	@FLAGS='$(TRACK_CFLAGS)'; \
	if test x"$$FLAGS" != x"`cat TSNIF-CFLAGS 2>/dev/null`" ; then \
		echo "$$FLAGS" >TSNIF-CFLAGS; \
	fi
