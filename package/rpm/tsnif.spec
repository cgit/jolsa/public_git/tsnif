Name:		tsnif
Version:	0.1
Release:	2%{?dist}
Summary:	tsnif - terminal sniffer/logger/replayer
Group:		Environment/Shells
License:        GPLv3+
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root
URL:            http://people.redhat.com/jolsa/tsnif
Source:         http://people.redhat.com/jolsa/tsnif/dl/%{name}-%{version}.tar.bz2
BuildRequires:  libnl-devel
Requires:	libnl

%description
tsnif - terminal sniffer/logger/replayer

%prep
%setup -q

%build
autoconf
%configure
make V=1

%install
rm -rf %{buildroot}
make install ROOTDIR=%{buildroot} V=1

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc README
%{_bindir}/tsnif
%{_bindir}/tsnifd
%{_bindir}/tsnif-replay

%changelog
* Tue Apr 14  2010 Jiri Olsa <jolsa@redhat.com> 0.1-2
- new source release, tsnifd

* Tue Apr 06  2010 Jiri Olsa <jolsa@redhat.com> 0.1-1
- initial package
